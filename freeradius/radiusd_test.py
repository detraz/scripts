#! /usr/bin/env python
# ⁻*- coding: utf-8 -*-
#
# Draft de fichier d'authentification
#
# Ce fichier contient la définition de plusieurs fonctions d'interface à freeradius
# qui peuvent être appelées (suivant les configurations) à certains moment de
# l'éxécution.
# 
# Une telle fonction prend un uniquement argument, qui est une liste de tuples
# (clé, valeur)
# et renvoie un triplet dont les composantes sont :
# * le code de retour (voir radiusd.RLM_MODULE_* )
# * un tuple de couples (clé, valeur) pour les valeurs de réponse
#   (access ok et autres trucs du genre)
# * un tuple de couples (clé, valeur) pour les valeurs internes à mettre à jour
#   (mot de passe par exemple)
#
# Voir des exemples plus complets ici:
# https://github.com/FreeRADIUS/freeradius-server/blob/master/src/modules/rlm_python/

import radiusd

def instantiate(p):
    """Appelé lors de l'instantiation du module"""
    print "*** instantiate ***"
    print p

def authorize(p):
    """Section authorize
    Éxécuté avant l'authentification proprement dite. On peut ainsi remplir les
    champs login et mot de passe qui serviront ensuite à l'authentification
    (MschapV2/PEAP ou MschapV2/TTLS)"""
    print "*** authorize ***"
    radiusd.radlog(radiusd.L_INFO, '*** radlog call in authorize ***')
    print p
    # À des fins de test, mon mot de passe est bidon
    mdp = 'prout'
    return (radiusd.RLM_MODULE_UPDATED,
	  (),
          (("Cleartext-Password", mdp),),
          #(('Session-Timeout', str(sessionTimeout)),),
          #(('Auth-Type', 'python'),),
          )
    return radiusd.RLM_MODULE_OK

def preacct(p):
    print "*** preacct ***"
    print p
    return radiusd.RLM_MODULE_OK

def accounting(p):
    print "*** accounting ***"
    radiusd.radlog(radiusd.L_INFO, '*** radlog call in accounting (0) ***')
    print
    print p
    return radiusd.RLM_MODULE_OK

def pre_proxy(p):
    print "*** pre_proxy ***"
    print p
    return radiusd.RLM_MODULE_OK

def post_proxy(p):
    print "*** post_proxy ***"
    print p
    return radiusd.RLM_MODULE_OK

def post_auth(p):
    """Appelé une fois que l'authentification est ok.
    On peut rajouter quelques éléments dans la réponse radius ici.
    Comme par exemple le vlan sur lequel placer le client"""

    print "*** post_auth ***"
    print p
    # Utiliser RLM_MODULE_OK ?
    return (radiusd.RLM_MODULE_UPDATED,
        (   
            ("Tunnel-Type", "VLAN"),
            ("Tunnel-Medium-Type", "IEEE-802"),
            ("Tunnel-Private-Group-Id", 42),
        ),
    )

def recv_coa(p):
    print "*** recv_coa ***"
    print p
    return radiusd.RLM_MODULE_OK

def send_coa(p):
    print "*** send_coa ***"
    print p
    return radiusd.RLM_MODULE_OK


def detach():
    """Appelé lors du déchargement du module (enfin, normalement)"""
    print "*** goodbye from example.py ***"
    return radiusd.RLM_MODULE_OK
