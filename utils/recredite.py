#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Pour recréditer facilement des jobs échoués. """

usage = u"""Usage : recredite.py JID1 JID2 JID3
ou recredite.py --csv FILE.CSV

Pour chaque job, va chercher le montant dans l'historique et recrédite l'adhérent.
Avec l'option --csv, parse un journal de l'imprimante et recrédite les jobs NG."""

import re
import time
import csv
import sys
if not "/usr/scripts" in sys.path:
    sys.path.insert(0, "/usr/scripts")

import lc_ldap.shortcuts

def get_adh_montant(jid, conn=None):
    """Récupère l'adhérent à recréditer et le montant correspondants à ce jid."""
    if conn == None:
        conn = lc_ldap.shortcuts.lc_ldap_admin()
    adh = conn.search(u"(historique=*debit*impression\(%s\)*)" % jid, mode='rw')
    if not adh:
        print "jid non trouvé dans l'historique LDAP : %s" % jid
        return None
    adh = adh[0]
    ligne = [l.value for l in adh["historique"] if "impression(%s)" % jid in l.value][0]
    montant = re.match(u".*debit (?P<montant>[0-9]+\.[0-9]+) Euros", ligne).groupdict()
    montant = float(montant["montant"])
    return (adh, montant)

def recredite(adh, montant, jid):
    """Effectue le crédit, sauf si il a déjà été fait."""
    # On vérifie que le remboursement n'a pas déjà été fait
    deja = [l.value for l in adh["historique"] if "remboursement(%s)" % jid in l.value]
    if deja:
        print "[jid=%s] %s (%s) déjà recrédité !" % (jid, adh["uid"][0], adh.dn.split(",")[0])
        return
    timestamp = time.strftime("%d/%m/%Y %H:%M")
    historique = "%s, %s : credit %.2f Euros [remboursement(%s)]" % (jid, timestamp, lc_ldap.shortcuts.current_user, montant, jid)
    adh["historique"].append(historique)
    solde = adh["solde"][0].value
    adh["solde"][0] = solde + montant
    print "%s (%s) recrédité de %.2f euros (nouveau solde : %s)" % (adh["uid"][0], adh.dn.split(",")[0], montant, adh["solde"][0])
    adh.save()

def parse_csv(filename):
    """Parse le .csv du journal de l'imprimante pour en extraire les jid des jobs en NG."""
    with open(filename) as f:
        csvreader = csv.reader(f)
        jobs = [l[2] for l in csvreader if l and l[1] == 'NG']
    jids = [int(j[1:].split(":")[0]) for j in jobs]
    return jids
    

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print usage
        sys.exit(1)
    if "--csv" in sys.argv:
        jids = parse_csv(sys.argv[2])
        print "jids parsés : %s" % jids
    else:
        jids = [int(jid) for jid in sys.argv[1:]]
    conn = lc_ldap.shortcuts.lc_ldap_admin()
    for jid in jids:
        jid = int(jid)
        adh, montant = get_adh_montant(jid, conn)
        recredite(adh, montant, jid)
