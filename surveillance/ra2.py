#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

# Réecriture complète de ra.py
# On fait tourner ramond. Dès qu'une mac non autorisée fait une ra, 
# ce script s'execute et pose une blackliste sur la machine. 
# Prise en compte du nouveau binding.
# Envoie de mails au propriétaire et à disconnect
# Gabriel Détraz <detraz@crans.org>

import os
import sys
import time
import subprocess

# On importe les scripts Crans
import lc_ldap.shortcuts
from gestion import mail
from utils.sendmail import actually_sendmail

# On règle le nombre de RA admissibles par jour par machine:
TOL = 3

# Chemin du fichier des logs
LOG = '/localhome/detraz/pirate2.txt'

# On ouvre une connexion LDAP à la base de test une fois pour toute.
conn = lc_ldap.shortcuts.lc_ldap_test()

Mac_ra = os.getenv('SOURCE_MAC')
#Mac_ra=u'48:d7:05:d3:0c:af'
#print Mac_ra

tm = time.strftime('%d/%m/%y',time.localtime())

# Logs des macs capturées, avec la date
with open('/localhome/detraz/pirate2.txt', 'a') as f:
        print >>f, Mac_ra, tm

# On cherche la machine correspondante
machine = conn.search(u'(macaddress=%s)' % Mac_ra,mode="rw")

# On agit que si la machine est connu
if machine <> []:
    md=machine[0]
#    print md

    # Si ca fait moins de TOL, on laisse passer :
    p = subprocess.Popen(['grep','-c',Mac_ra+' '+tm,LOG],stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    print stdout
    if int(stdout) <= TOL:
        sys.exit(1)    

    # Sécurité : on évite de poser une bl à un serveur
    if isinstance(md.proprio(), lc_ldap.objets.AssociationCrans): 
        sys.exit(1)

    # Cohérence : si il y a déjà un bl, on arrète
    bl = md.blacklist_actif()
#    print bl 
    for x in bl:
#        print x['type']
        if x['type']=='ipv6_ra':
            sys.exit(1)       
 
    adh = md.proprio()

#    print md.proprio()['prenom'][0]
#    print md.proprio().get_mail()

    #On pose la blackliste sur la machine incriminée

    md.blacklist('ipv6_ra',u'auto ra.py : router advertisement non autorisée',debut='now',fin='-') 
    md.save()


    # On envoie une notification à disconnect et à la personne :

    From = 'disconnect@crans.org'
    To = adh.get_mail()
    Cc = 'disconnect@crans.org'
    name = adh.ldap_name
    mach = unicode(md['host'][0])
    if name=='adherent':
        tname = unicode(adh['prenom'][0]) + " " + unicode(adh['nom'][0])
    elif name=='club':
        tname = unicode(adh['nom'][0])
    mailtxt=mail.generate('deconnex_ra', {
            'To': To,
            'From': From,
            'tname': tname,
            'mach': mach,
    })

    actually_sendmail(From, (To,Cc), mailtxt)
