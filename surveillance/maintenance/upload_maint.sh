#!/bin/bash

psql -d filtrage -f /usr/scripts/surveillance/maintenance/delete.sql;
/usr/scripts/surveillance/maintenance/check_activity.py;
psql -d filtrage -f /usr/scripts/surveillance/maintenance/vacuum.sql 2>&1 > /dev/null
