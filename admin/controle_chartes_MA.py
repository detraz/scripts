#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) Stéphane Glondu, Alexandre Bos, Michel Blockelet
# Licence : GPLv2

u"""Ce script permet au secrétaire de repérer plus facilement les membres
actifs qui n'ont pas signé la charte du même nom.

Utilisation :
%(prog)s {liste|modif|spam} [--debug <adresse>]

L'unique option est :
  --debug <adresse>     envoyer tous les mails à l'<adresse> indiquée, plutôt
                        qu'aux vrais destinataires
Les commandes sont :
  * liste               énumérer les membres n'ayant pas signé la charte
  * modif               modifier les membres actifs n'ayant pas signé la charte
  * spam                envoie des mails de rappel pour les chartes
"""


import sys, os, re
sys.path.append('/usr/scripts/gestion')
import config
import config.mails
from email_tools import send_email, parse_mail_template

# Fonctions d'affichage
from affich_tools import coul, tableau, prompt, cprint

# Importation de la base de données
from ldap_crans import crans_ldap, ann_scol
db = crans_ldap()

# Lors des tests, on m'envoie tous les mails !
from socket import gethostname
debug = False

if __name__ == '__main__':
    if len(sys.argv) > 3 and sys.argv[-2] == '--debug':
        debug = sys.argv[-1]
        sys.argv.pop()
        sys.argv.pop()

if debug:
    cprint(u'Mode debug, tous les mails seront envoyés à %s.' % debug)


def _controle_interactif_adherents(liste):
    """
    Contrôle interactif des adhérents de la liste.
    Retourne (nb_OK, nb_pas_OK).
    """

    restant = len(liste)
    if restant == 0:
        return 0, 0
    
    cprint(u'\nContrôle des membre actifs' , 'cyan')
    cprint(u"Pour chaque entrée, il faut taper 'o' ou 'n' (défaut=n).")
    cprint(u"Une autre réponse entraîne l'interruption du processus.")
    cprint(u"Le format est [nb_restant] Nom, Prénom (aid).")
    cprint(u"")
    
    nb = 0
    for a in liste:
        valeur = a.charteMA()
        if valeur:
            suggestion = 'o'
        else:
            suggestion = 'n'
        ok = prompt(u'[%3d] %s, %s (%s) ?'
                    % (restant, a.nom(), a.prenom(), a.id()), suggestion, '').lower()
        restant -= 1
        if ok == 'o':
            nb += 1
            if a.charteMA() == False :
                modifiable = db.search('aid=%s' % a.id(), 'w')['adherent'][0]
                if modifiable._modifiable:
                    modifiable.charteMA(True)
                    cprint(modifiable.save())
                else:
                    cprint(u'Adhérent %s locké, réessayer plus tard' % modifiable.Nom(), 'rouge')
        elif ok == 'n':
            if a.charteMA() == True:
                modifiable = db.search('aid=%s' % a.id(), 'w')['adherent'][0]
                if modifiable._modifiable:
                    modifiable.charteMA(False)
                    cprint(modifiable.save())
                else:
                    cprint(u'Adhérent %s locké, réessayer plus tard' % modifiable.Nom(), 'rouge')
        else:
            cprint(u'Arrêt du contrôle %s des membres actifs' % explicite, 'rouge')
            break

    return nb, len(liste)-nb


def liste_charte_nok():
    """Retourne la liste des membres actifs qui n'ont pas signé la charte."""
    liste_actifs = db.search('droits=*')['adherent']
    liste_nok = []
    for adh in liste_actifs:
        if (len([droit for droit in adh.droits()
                if droit not in ['Multimachines', 'Webradio']]) > 0
                and not adh.charteMA()):
            liste_nok.append(adh)
    return liste_nok

def controle_interactif():
    """
    Procédure interactive de contrôle des chartes de membres actifs.
    """
    todo_list = liste_charte_nok()
    
    # Tri de la liste des adhérents selon nom, prénom
    # Ça peut se faire plus facilement en Python 2.4 avec l'argument key
    todo_list.sort(lambda x, y: cmp((x.nom(), x.prenom()), (y.nom(), y.prenom())))

    # Zou !
    ok, nok = _controle_interactif_adherents(todo_list)
    
    cprint(u'\nRécapitulatif des nouveaux contrôles :', 'violet')
    liste = [[u'membres actifs', str(ok), str(nok)]]
    cprint(tableau(liste,
                  titre = [u'Catégorie', u'OK', u'pas OK'],
                  largeur = [15, 10, 10]))

def spammer():
    # On envoie un mail à chacun des membres actifs qui n'ont pas donné le papier
    todo_list = liste_charte_nok()

    if todo_list:
        from smtplib import SMTP
        connexion = SMTP()
        if gethostname().split(".")[0] == 'redisdead':
            connexion.connect("localhost")
        else: connexion.connect("redisdead.crans.org")
        print "Envoi des mails de rappel pour les chartes des membres actifs"
    
        for adh in todo_list:
            to = adh.email()
            print to
            if not debug:
                data = config.mails.txt_charte_MA % {'From' : u"ca@crans.org", 'To' : to}
                connexion.sendmail("ca@crans.org",to,data.encode('utf-8'))
          
def __usage(message=None):
    """ Comment ça marche ? """
    cprint(__doc__ % { 'prog': sys.argv[0] })
    if message:
        cprint(message)
    sys.exit(1)


if __name__ == '__main__' :
    # Utilisation depuis la ligne de commande
    if len(sys.argv) <= 1:
        __usage()
    elif sys.argv[1] == 'liste':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de liste')
        print "Liste des membres actifs n'ayant pas signé la charte :"
        for adh in liste_charte_nok():
            print adh.Nom()
    elif sys.argv[1] == 'modif':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de modif')
        controle_interactif()
    elif sys.argv[1] == 'spam':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de spam')
        spammer()
    else:
        __usage(u'Commande inconnue : %s' % sys.argv[1])

    sys.exit(0)


# pydoc n'aime pas l'unicode :-(
