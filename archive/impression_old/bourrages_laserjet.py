#!/usr/bin/env python
# -*- encoding: iso8859-15 -*-
# 
# bourrages.py : r�cup�ration des fichiers de logs de bourrage l'imprimante.
# 
# Stocke les logs de l'imprimante au fur et � mesure
#
# Copyright (c) 2008 Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
#

import urllib
import sys
import re

nom_fichier_log = "/var/log/bourrages.log"
URL = "https://laserjet.adm.crans.org/hp/device/this.LCDispatcher?dispatch=html&cat=0&pos=3"

span_rex = re.compile(r'<span[^>]+class="hpPageText"[^>]*>(?P<contenu>[^<]+)</span>', re.IGNORECASE)

try:
    f = open(nom_fichier_log)
except:
    sys.stderr.write("Erreur d'ouverture du fichier de logs en lecture...\n")
    lastpage = 0
else:
    lastpage = f.readlines()[-1].strip().split()[0]
    f.close()

try:
    lignesimpr = urllib.urlopen(URL).readlines()
except:
    sys.stderr.write("Probl�me de lecture de la page d'impression...\n")
else:
    lignes_recup = [span_rex.match(ligne).group("contenu") for ligne in lignesimpr if span_rex.match(ligne)]
    
    premier_indice = lignes_recup.index('Num\xe9ro') + 4
    erreurs = []

    for i in xrange(premier_indice, len(lignes_recup), 4):
        erreurs.append(tuple(lignes_recup[i+1:i+4]))
    
    try:
        fichier_log = open(nom_fichier_log, 'a')
    except:
        sys.stderr.write("Erreur d'ouverture du fichier de logs en �criture...\n")
    else:
        for erreur in reversed(erreurs):
            if erreur[0] > lastpage:
                fichier_log.write("%s %s %s\n" % erreur)

        fichier_log.close()
