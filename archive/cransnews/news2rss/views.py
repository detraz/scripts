# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
import nntplib
import email

def news2rss(request, group):
    news = nntplib.NNTP('news.crans.org')
    resp, count, first, last, name = news.group(group)
    ## faire: renvoyer 404 si existe pas NNTPTemporaryError
    subs = news.xhdr('subject', str(int(last)-10) + '-' + last)[1]
    response = ""
    for id, sub in subs[-10:]:
        response += sub
    message = news.xhdr(group, last)
    return HttpResponse(response)
