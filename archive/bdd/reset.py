#!/usr/bin/python 

import connection

from proprio import Proprio
from proprio import HistProprio
from comptes import CompteUnix
from comptes import GroupeUnix
from comptes import Attribut
from comptes import Permission

erreurs = 0

try:
    Proprio.dropTable(cascade=True)
except:
    erreurs += 1

Proprio.createTable()

try:
    HistProprio.dropTable(cascade=True)
except:
    erreurs += 1
HistProprio.createTable()

try:
    CompteUnix.dropTable(cascade=True)
except:
    erreurs += 1
CompteUnix.createTable()



try:
    GroupeUnix.dropTable(cascade=True)
except:
    erreurs += 1
GroupeUnix.createTable()


try:
    Attribut.dropTable(cascade=True)
except:
    erreurs += 1
Attribut.createTable()

try:
    Permission.dropTable(cascade=True)
except:
    erreurs += 1
Permission.createTable()

print 'erreurs : ', erreurs