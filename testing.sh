# Sourcé par python.sh en environnement de test, à modifier suivant vos envies
# (et les tests que vous souhaitez faire)

# Utiliser ldap local
export DBG_LDAP=1

# Utiliser l'annuaire pgsql local
export DBG_ANNUAIRE=1

# Mails auto, plusieurs valeurs:
# * print: affiche le mail au lieu de l'envoyer
# * une adresse mail: envoie tous les mails à cette adresse mail au lieu de
#   la vraie
# NB: noter que pour le moment, cela ne marche pas avec tous les scripts.
# Attention à ne pas envoyer de mails aux adhérents par erreurs !
export DBG_MAIL=`whoami`+test@crans.org

# Serveur freeradius de test ?
export DBG_FREERADIUS=1

# Imprime pour de vrai ?
export DBG_PRINTER=1

# Un dossier où trouver une version alternative des secrets (fichier par
# fichier)
export DBG_SECRETS=/etc/crans/dbg_secrets/`whoami`/

# Pour la wifimap
export DBG_WIFIMAP_DB=$CPATH/var/wifi_xml
