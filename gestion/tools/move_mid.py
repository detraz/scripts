#!/usr/bin/env python
# -*- coding: utf8 -*-

import sys
import ldap
sys.path.append('/usr/scripts/gestion')
import ldap_crans

def move(machine, mid, conn):
    """
    Déplace l'objet ldap machine vers le mid considéré
    """
    
    conn.conn.rename_s(machine.dn, "mid=%s" % (mid))
    machine.dn = "mid=%s,%s" % (mid, conn.base_dn)
    machine._data["mid"] = [ "%s" % (mid) ] 
    machine.save()

def construct_mids(from_mid, conn):
    """
    Cherche un mid libre dans la base LDAP en partant de 256
    (autant ne pas mettre de garbage au milieu des serveurs
    """
    
    if from_mid == None or from_mid == '':
        from_mid = 256

    filtre_idn = ldap_crans.Machine.filtre_idn
    liste = conn.conn.search_s(conn.base_dn, ldap.SCOPE_SUBTREE, filtre_idn)
    liste_mid = [0]

    for element in liste:
        r = element[0].split(',')[0]
        if r[:3] != "mid": continue
        liste_mid.append(int(r[4:]))

    liste_mid.sort()
    return list(set(range(from_mid, 65536)).intersection(liste_mid))

if __name__ == "__main__":
    conn = ldap_crans.crans_ldap()
    liste_mid = construct_mids(256, conn)

    while True:
        last = liste_mid[-1]
        libre = 256

        while libre in liste_mid:
            libre = libre + 1

        print libre, last
        if libre >= last:
            break

        else:
            machine = conn.search('mid=%s' % (last))['machine'][0]
            prev_mid = machine.id()
            move(machine, libre, conn)
            print "Machine mid=%s déplacée au mid=%s" % (prev_mid, libre)

        liste_mid.remove(last)
        liste_mid.append(libre)
        liste_mid.sort()

    print liste_mid
