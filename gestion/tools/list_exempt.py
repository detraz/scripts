#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/gestion')

from ldap_crans import crans_ldap, decode

db = crans_ldap()
machines = db.search('exempt=*')['machine']

txts = []

for m in machines :
    
    # texte pour la machine
    txt =  u'' 
    txt += u'Propriétaire : %s\n' % m.proprietaire().Nom()
    txt += u'Machine      : %s\n' % m.nom()
    txt += u'destination  : %s\n' % ', '.join(m.exempt())

    txts.append(txt.strip())

print '\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(txts)
