#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
#
# Parser for firewall service.
#
# Author  : Pierre-Elliott Bécue <becue@crans.org>
# Licence : GPLv3
# Date    : 15/06/2014
"""
This is the parser for firewall service.
"""

import lc_ldap.attributs
from gestion.trigger.host import record_parser

@record_parser(lc_ldap.attributs.macAddress.ldap_name, lc_ldap.attributs.ipHostNumber.ldap_name)
def send_mac_ip(body, diff):
    """Computes mac_ip data to send from body and diff

    """
    macs = tuple([body[i].get(lc_ldap.attributs.macAddress.ldap_name, [''])[0] for i in xrange(1, 3)])
    ips = tuple([body[i].get(lc_ldap.attributs.ipHostNumber.ldap_name, [''])[0] for i in xrange(1, 3)])

    # Mise à jour du parefeu mac_ip
    if not macs[0]:
        # Création d'une nouvelle machine.
        fw = {'add': [(macs[1], ips[1])]}
    elif not macs[1]:
        # Destruction d'une machine.
        fw = {'delete': [(macs[0], ips[0])]}
    else:
        # Mise à jour.
        fw = {'update': [(macs[0], ips[0], macs[1], ips[1])]}
    return ("firewall", ("mac_ip", fw))

