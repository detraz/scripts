Welcome!
========


If you read this email, your registration at the association Cr@ns was successful.

Cr@ns is a volunteer-run organization for and by students living on campus.

Its active members are involved in the maintenance of the network , membership 
registration (*câblages*) , and several other services available to all members 
among which:

  * Wired and wireless[1] Internet connection;
  * a wiki, gathering plenty of various information about life around Cachan [2];
  * a newsgroup server [3];
  * an IRC server [4];
  * a Jabber IM server [5];
  * a mail service : a reliable mail address @crans.org for a lifetime [6];
  * a personal space of *9GB** on the server members [7];
  * a print service 24 hours a day, every day, A3 or A4, color or greyscale, with or 
    without clip, at cost [8];
  * TV on the network [9].

To ensure the proper functioning of these services, it is necessary that each 
member respects the rules [10] agreed at registration.


The instructions for use of the Cr@ns services is available at:
<https://wiki.crans.org/CransPratique>. We urge you to refer to it, to learn 
how to use the various services, or to resolve any problems encountered.

Each member interested in the functioning of the association may contact 
the technical team at <nounous@crans.org> or the administrative team
at <ca@crans.org> and / or attend public meetings [11].

**No prior technical knowledge is required to participate!**

**Note**: An authentication is require to access news and wiki
from outside the campus:

 * For access to the wiki from outside, you must create an account [12] .
 * For access to the news from outside , you must identify
   ( User: Vivelapa / Password: ranoia! )


{% include 'bienvenue/body/links' %}

-- 

The active members.

PS : It is recommended that you keep this email
