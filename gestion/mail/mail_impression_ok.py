#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
"""Envoi de mail automatique pour signaler la fin d'impression à un
adhérent. Le script se connecte à l'interface d'impression pour récupérer
la liste des dernières tâches imprimées.
Ce script est lancé par un cron toutes les dix minutes sur zamok. Pour éviter
de notifier plusieurs fois de la même fin d'impression, on ne balaie dans
la liste que le dernier intervalle (bornes entières) de dix minutes"""

import BeautifulSoup
import sys
import requests
import re
import datetime
import time
import smtplib

from gestion.affich_tools import cprint
from gestion import mail
from utils.sendmail import actually_sendmail
from lc_ldap import shortcuts
import impression.digicode as digicode

URL_JOBLIST = 'https://imprimante.adm.crans.org/hp/device/JobLogReport/Index'
CA = '/etc/ssl/certs/cacert.org.pem'

RE_JOB_NAME = re.compile('JobLogName_')
RE_JOB_STATUS = re.compile('JobLogStatus_')
RE_JOB_DATE = re.compile('JobLogDate_')

try:
    req = requests.get(URL_JOBLIST, verify=CA)
except requests.exceptions.ConnectionError:
    print '''L'imprimante est injoignable. Une intervention manuelle peut être nécessaire
Le script de notification d'impressions'''
    exit(1)

if req.status_code!=200:
    print '''L'imprimante est injoignable. Une intervention manuelle peut être nécessaire
Le script de notification d'impressions'''
    exit(1)

doc = BeautifulSoup.BeautifulSoup(req.text)
con = shortcuts.lc_ldap_readonly()

jobs = doc.findAll(attrs={'class': 'PrintJobTicket'})

VERB=False

# Dernier intervalle de dix minutes, en bornes entières, qui vient de se
# terminer
now = datetime.datetime.now()
if sys.argv[1:]:
    now = datetime.datetime.strptime(sys.argv[1], '%d/%m/%Y %H:%M:%S')
    VERB=True
    print "Overridden date: %s" % str(now)
fin = now.replace(second=0, minute=(now.minute/10)*10, microsecond=0)
debut = fin - datetime.timedelta(minutes=10)

success=dict()
clubs=dict()
echecs=dict()
for job in jobs:
    # Fin de parsing
    uid = job.find(attrs={'id': RE_JOB_NAME}).text
    status = job.find(attrs={'id': RE_JOB_STATUS}).text
    date = job.find(attrs={'id': RE_JOB_DATE}).text
    date = datetime.datetime.strptime(date, '%Y/%m/%d %H:%M:%S')

    # Hors intervalle: on drope
    if date < debut or date >= fin:
        continue
    split = uid.split(u':')
    if status != "Success":
        # Préviens les imprimeurs si une impression a échoué,
    # pas de fioritures ici, boucle assez basique
        if len(split) < 3:
            name = u'Impression à la main'
            task = uid
            if name not in echecs:
                echecs[name] = {'task': []}
            echecs[name]['task'].append(task)
        else:
            nb = split[0]
            name = split[1]
            task = u':'.join(split[2:])
            if name not in echecs:
                echecs[name] = {'task': []}
            echecs[name]['task'].append(task)

    if status != "Success":
        continue
    # Dans le cas d'un success,
    if len(split) < 3:
        continue
    # Boucle qui s'exécute normalement si la typographie dans
    # les logs d'impression est repectée (:) (sinon c'est un
    # impression manuel donc drop)
    nb = split[0]
    name = split[1]
    task = u':'.join(split[2:])
    if u'@' in name:
        #Seuls les clubs ont un @ dans leur alias, donc boucle
        # dédiée au clubs
        [name, club] = name.split(u'@', 1)
        if club not in clubs:
            clubs[club] = {'task': []}
        clubs[club]['task'].append(task)
    if name not in success:
        success[name] = {'task': []}
    success[name]['task'].append(task)

#Section consacrée à l'envoi : partie 1 pour les adh, partie 2 pour les clubs
#To = 'detraz@crans.org'
From = 'impression@crans.org'
e = 0
a = 0

for name in success:
    ad = con.search(u'(uid=%s)' % name)
    if ad <> []:
        a = a + 1
        adh = ad[0]
        To = name + u'@crans.org'
        tname = unicode(adh['prenom'][0]) + " " + unicode(adh['nom'][0])
        codes = [x[0] + u'#' for x in digicode.list_code(name)]
        if not codes:
            codes = [digicode.gen_code(name) + u'#']
        if VERB:
            print (u"Envoi du mail à %s" % To)
        mailtxt=mail.generate('mail_impression_ok', {
            'To': To,
            'From': From,
            'tname': tname,
            'taches': u', '.join(success[name]['task']),
            'codes': u', '.join(codes)
        })
        #print mailtxt.as_string()
        actually_sendmail(From, (To,), mailtxt)
    else:
        e = e+1

for club in clubs:
    a = a + 1
    tname = club
    To = club + u'@crans.org'
    codes = [x[0] + u'#' for x in digicode.list_code(club)]
    if not codes:
        codes = [digicode.gen_code(club) + u'#']
    if VERB:
        print (u"Envoi du mail à %s" % To)
    mailtxt=mail.generate('mail_impression_ok', {
        'To': To,
        'From': From,
        'tname': tname,
        'taches': u', '.join(clubs[club]['task']),
        'codes': u', '.join(codes),
    })
    #print mailtxt.as_string()
    actually_sendmail(From, (To,), mailtxt)

for name in echecs:
    ad = con.search(u'(uid=%s)' % name)
    To = 'impression@lists.crans.org'
    if ad <> []:
        adh = ad[0]
        tname = unicode(adh['prenom'][0]) + " " + unicode(adh['nom'][0])
    else:
        tname = name
    mailtxt=mail.generate('mail_impression_ratee', {
        'To': To,
        'From': From,
        'tname': tname,
        'taches': u', '.join(echecs[name]['task']),
    })
    #print mailtxt.as_string()
    actually_sendmail(From, (To,), mailtxt)



if e>0:
    print "Des problèmes sont survenus entre %s et %s" % (debut, fin)
    print "Nombre total de mails envoyés :"
    print a
    print "Nombre de mails non envoyés faute de résultats LDAP :"
    print e
#print len(success)
