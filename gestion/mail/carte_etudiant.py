#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

import sys
import datetime
import pytz
from gestion import config
from gestion.affich_tools import cprint
from gestion import mail as mail_module
import lc_ldap.shortcuts

# Attention, si à True envoie effectivement les mails
SEND=False
# TODO ces deadlines devraient être dans config.py
deadline=datetime.datetime(config.ann_scol, 11, 6)
fin_conn = datetime.datetime(config.ann_scol, 11, 1, tzinfo=pytz.UTC)
fin_conn = fin_conn.strftime('%Y%m%d%H%M%S%z')
ldap_filter=u"""(&
  (finConnexion>=%(fin_conn)s)
  (!(carteEtudiant=*))
  (!(etudes=Personnel ENS))
  (aid=*)
)""" % {'fin_conn': fin_conn, }

conn=lc_ldap.shortcuts.lc_ldap_readonly()
mailaddrs=set()
for adh in conn.search(ldap_filter):
    mail = adh.get_mail()
    if not mail:
        print "Skip %r (no valid mail)" % adh
        continue
    mailaddrs.add(mail)

print "Va envoyer le message à %s personnes." % len(mailaddrs)
if not SEND:
    print "Mettre la variable SEND à True effectuer l'envoi"
print "Appuyer sur entrée pour la génération"
raw_input()

echecs=[]
with mail_module.ServerConnection() as conn_smtp:
    for To in mailaddrs:
        cprint(u"Envoi du mail à %s" % To)
        params = {
            'deadline': deadline,
            'To': To,
            'lang_info': 'English version below',
        }
        mailtxt=mail_module.generate('carte_etudiant', params).as_string()
        try:
            # TODO DBG_MAIL
            if SEND:
                conn_smtp.sendmail("cableurs@crans.org", (To,), mailtxt)
        except:
            cprint(u"Erreur lors de l'envoi à %s " % To, "rouge")
            echecs.append(To)

if echecs:
    print "\nIl y a eu des erreurs :"
    print echecs
