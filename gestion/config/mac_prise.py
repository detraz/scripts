#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Variables de configuration pour mac_prises. """

#: Pour spammer disconnect@lists.crans.org à chaque analyse, mettre à True
hargneux = False

#: Mail de destination des infos.
if hargneux:
    mail_to = "test@lists.crans.org"
else:
    mail_to = "disconnect@lists.crans.org"

#: Si pour une chambre donnée, il y a plus de 300 entrées filaires
#: n'appartenant pas à l'adhérent propriétaire de la mac, on prévient.
max_inconnues_par_jour = 480

#: Titre utilisé dans le corps du message
titre_mac_inconnue = u"Repérage de macs potentiellement non désirées dans les chambres suivantes."

#: Pour la recherche dans postgres
delay = { 'instantanne': '2 min',
          'moyen': '30 min',
          'journalier': '1 day',
        }

#: Contient trois dictionnaire. Le paramètre mac signifie "combien de chambres doivent voir la même mac pour que ça soit suspect"
#: Le paramètre chambre signifie "combien de macs doivent traverser une même chambre pour que ça soit suspect"
suspect = { 'instantanne':2,
            'moyen':2,
            'journalier':2,
          }

#: Le point central des analyses.
rapport_suspect = { 'instantanne':0.51,
                    'moyen':0.4,
                    'journalier':0.2,
                  }

#: Titre des paragraphes suspects
titre_suspect = { 'instantanne':u"Macs se baladant un peu trop entre les chambres (instantanneanné)",
                  'moyen':u"Macs se baladant un peu trop entre les chambres (délai moyen)",
                  'journalier':u"Macs s'étant peut-être un peu trop baladées aujourd'hui",
                }
