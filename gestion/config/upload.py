#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Définitions des variables pour le contrôle d'upload. """

#: liste des exemptions générales
exempt = [ ['138.231.136.0/21', '138.231.0.0/16'],
           ['138.231.148.0/22', '138.231.0.0/16'] ]

#: limite soft
soft = 1024 # Mio/24h glissantes

#: limite hard
hard = 8192 # Mio/24h glissantes

#: max déconnexions
max_decos = 7

#: envoyer des mails à disconnect@ en cas de dépassement soft ?
disconnect_mail_soft = False
#: envoyer des mails à disconnect@ en cas de dépassement hard ?
disconnect_mail_hard = True

#: expéditeur des mails de déconnexion
expediteur = "disconnect@crans.org"

#: Seuil pour report dans statistiques.py
stats_upload_seuil = 200 * 1024 * 1024 # Mio
pretty_seuil = "%s Mio" % (stats_upload_seuil/1024/1024,)
