#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Variables de configuration pour le firewall """

import datetime
from .feries import is_ferie

#: Interfaces réseaux des machines ayant un pare-feu particulié
dev = {
    'komaz': {
        'out' : 'ens',
        'wifi' : 'crans.3',
        'fil' : 'crans',
        'app' : 'crans.21',
        'adm' : 'crans.2',
        'tun-soyouz' : 'tun-soyouz'
    },
    'odlyd': {
        'out' : 'ens',
        'wifi' : 'crans.3',
        'fil' : 'crans',
        'app' : 'crans.21',
        'adm' : 'crans.2',
        'tun-soyouz' : 'tun-soyouz'
    },
    'zamok': {
        'fil' : 'crans',
        'adm' : 'crans.2'
    },
    'routeur': {
        'fil' : 'eth0',
        'adm' : 'eth1',
        'accueil' : 'eth2',
        'isolement' : 'eth3',
        'app' : 'eth4'
    },
}

#: Pour marquer les paquets
mark = { 'https-radin': '0x3',
         'https-gratuit' : '0x3',
         'proxy'      : '0x2',
         'secours' : '0x4',
         'bittorrent' : '0x1' }

#: Valeur du masque utilisé pour créer un arbre dans les filtres
mask = [24]

# Empiriquement, 95 correspond à un débit de 100Mbit/s
# sur des outils tels que munin
now=datetime.datetime.now()
if now.hour >= 6 and now.hour < 19 and now.weekday() < 5 and not is_ferie():
    #: Débit maximal autorisé
    debit_max = 150 # mbits per second en connexion de jour
    #: Est-ce qu'on est en connexion de jour ou de nuit/week-end ?
    debit_jour = True
else:
    #: Débit maximal autorisé
    debit_max = 500 # mbits per second en conn de nuit et du week-end
    #: Est-ce qu'on est en connexion de jour ou de nuit/week-end ?
    debit_jour = False

#Débit max en upload pour les déconnectés pour upload.
bl_upload_debit_max = 60 #kbytes per second

# Débit pour upload des gens en appartement ens
appt_upload_max = 1 # mbytes per second

# Debit appartement down max
# TODO : mettre en place dans komaz.py
appt_download_max = debit_max/10

#: Liste des réseaux non routables
reseaux_non_routables = [ '10.0.0.0/8', '172.16.0.0/12','198.18.0.0/15',
        '169.254.0.0/16', '192.168.0.0/16', '224.0.0.0/4', '100.64.0.0/10',
        '0.0.0.0/8','127.0.0.0/8','192.0.2.0/24','198.51.100.0/24','203.0.113.0/24',
        '192.0.0.0/29', '240.0.0.0/4', '255.255.255.255/32',
        ]

#: Ports ouverts à défaut pour les adhérents dans le pare-feu
ports_default = {
    'tcp' : {
        'input' :  [ '22' ],
        'output' :  [ ':24', '26:79', '80:134', '136', '140:444', '446:']
    },
    'udp' : {
        'input' :  [],
        'output' :  [ ':136','140:']
    }
}

srv_ports_default = {
    'tcp' : {
        'input' : [ '22' ],
        'output' : []
    },

    'udp' : {
        'input' : [],
        'output' : []
    }
}
