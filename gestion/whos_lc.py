#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
#
# Auteur : Pierre-Elliott Bécue <becue@crans.org>
# Licence : GPLv3
# Date : 01/09/2014

import sys
import argparse
from ldap import SIZELIMIT_EXCEEDED

import lc_ldap.shortcuts
import lc_ldap.objets
import lc_ldap.filter2 as lfilter
import lc_ldap.crans_utils
# To be developed
#import gestion.logger.Logger as Logger

ldap = lc_ldap.shortcuts.lc_ldap_readonly()

encoding = "utf-8"

def explore_db(args):
    """
    Utilise le contenu de args pour décider comment explorer la base de données.
    """
    data = search_ldap(args)
    data = limits(data, args)
    dataLen = sum([len(elem) for elem in data.itervalues()])
    if dataLen:
        for elem in data.itervalues():
            if len(elem) == 1:
                elem[0].display(historique=args.historique, blacklist=args.blacklist)
            else:
                print lc_ldap.printing.sprint_list(elem).encode(encoding)
                print "%s résultats" % len(elem)

def search_ldap(args):
    """
    Cherche et trie
    """
    data = {}
    if args.ldap:
        try:
            resultats = ldap.search(args.filtre.decode(encoding), sizelimit=args.limit)
        except SIZELIMIT_EXCEEDED:
            raise EnvironmentError("La limite de résultats LDAP (%s) a été dépassée. Vous pouvez l'augmenter avec l'option -l" % (args.limit,))
        for elem in resultats:
            if not data.has_key(elem.__class__.__name__):
                data[elem.__class__.__name__] = [elem]
            else:
                data[elem.__class__.__name__].append(elem)
    else:
        try:
            resultats = ldap.search(lfilter.human_to_ldap(args.filtre.decode(encoding)), sizelimit=args.limit)
        except SIZELIMIT_EXCEEDED:
            raise EnvironmentError("La limite de résultats LDAP (%s) a été dépassée. Vous pouvez l'augmenter avec l'option -l" % (args.limit,))
        for elem in resultats:
            if not data.has_key(elem.__class__.__name__):
                data[elem.__class__.__name__] = [elem]
            else:
                data[elem.__class__.__name__].append(elem)
    return data

def limits(data, args):
    """
    Applique les limitations dans la recherche.
    Les cas sont a priori conflictuels.
    """
    data_restricted = {}
    data_restricted.update(data)
    contentFilter = []
    if args.adherent:
        contentFilter = ["adherent"]
    elif args.club:
        contentFilter = ["club"]
    elif args.machine:
        contentFilter = ["machineFixe", "machineWifi", "machineCrans", "switchCrans", "borneWifi"]
    elif args.crans:
        contentFilter = ["machineCrans", "switchCrans", "borneWifi"]
    elif args.switch:
        contentFilter = ["switchCrans"]
    elif args.borne:
        contentFilter = ["borneWifi"]
    # Special cases.
    elif args.adm:
        contentFilter = []
        out = []
        for machine in data.get('machineCrans', []):
            if lc_ldap.crans_utils.find_rid_plage(machine['rid'][0].value)[0].startswith('adm'):
                out.append(machine)
        data_restricted = {'machineCrans' : out,}
    elif args.special:
        contentFilter = []
        out = []
        for machine in data.get('machineCrans', []):
            if lc_ldap.crans_utils.find_rid_plage(machine['rid'][0].value)[0].startswith('special'):
                out.append(machine)
        data_restricted = {'machineCrans' : out,}
    elif args.serveur:
        contentFilter = []
        out = []
        for machine in data.get('machineCrans', []):
            if lc_ldap.crans_utils.find_rid_plage(machine['rid'][0].value)[0].startswith('serveur'):
                out.append(machine)
        data_restricted = {'machineCrans' : out,}
    if contentFilter:
        data_restricted = {a: data.get(a, []) for a in contentFilter}
    return data_restricted

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Recherche dans la base des adhérents", add_help=False)
    parser.add_argument('-6', '--ipv6', help="Affiche les ipv6.", action="store_true")
    parser.add_argument('-A', '--adresse', help="Affiche l'adresse de l'adhérent.", action="store_true")
    parser.add_argument('-d', '--blacklist', type=int, help="Choix du nombre d'entrées blacklist à afficher pour les entrées détaillées.", action="store", default=10)
    parser.add_argument('-h', '--help', help="Affiche ce message et quitte.", action="store_true")
    parser.add_argument('-i', '--ipsec', help="Affichage de la clef wifi de la machine.", action="store_true")
    parser.add_argument('-l', '--limit', type=int, help="Modifier la taille limite de recherche dans la base LDAP", action="store", default=1000)
    parser.add_argument('-L', '--historique', type=int, help="Choix du nombre d'entrées d'historique à afficher pour les entrées détaillées.", action="store", default=10)
    parser.add_argument('-s', '--sshfp', help="Affiche les fingerprint SSH si elles existent.", action="store_true")
    parser.add_argument('-t', '--ldap', help="Utiliser les filtres tels que définis dans ldap", action="store_true")
    parser.add_argument('-v', '--verbose', help="Rend le script (très) verbeux.", action="store_true")
    parser.add_argument('filtre', type=str, nargs="?", help="Le filtre whos à utiliser")

    type_group = parser.add_mutually_exclusive_group(required=False)
    type_group.add_argument('-a', '--adherent', help="Limite l'affichage aux adhérents.", action="store_true")
    type_group.add_argument('--adm', help="Limite l'affichage aux machines adm.", action="store_true")
    type_group.add_argument('-b', '--borne', help="Limite l'affichage aux bornes.", action="store_true")
    type_group.add_argument('-c', '--club', help="Limite l'affichage aux clubs.", action="store_true")
    type_group.add_argument('--crans', help="Limite l'affichage aux machines crans.", action="store_true")
    type_group.add_argument('-m', '--machine', help="Limite l'affichage aux machines.", action="store_true")
    type_group.add_argument('--serveur', help="Limite l'affichage aux serveurs.", action="store_true")
    type_group.add_argument('--special', help="Limite l'affichage aux machines spéciales.", action="store_true")
    type_group.add_argument('--switch', help="Limite l'affichage aux switches (pas encore implémenté).", action="store_true")

    args = parser.parse_args()

    if args.help:
        parser.print_help()
        sys.exit(0)
    else:
        #logger = Logger()
        explore_db(args)
