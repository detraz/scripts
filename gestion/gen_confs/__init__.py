# -*- coding: utf-8 -*-

""" Package pour la génération des fichiers de conf

Copyright (C) Frédéric Pauget
Licence : GPLv2
"""

import sys, os, signal
import time, commands
from tempfile import NamedTemporaryFile
sys.path.append('/usr/scripts/gestion')
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

from gestion.affich_tools import *
from gestion.lock import *
from gestion import config


class gen_config(object) :
    """ Base pour toutes les classes de génération de fichiers de conf """
    base = None
    debug = 0
    _locked = 0
    __restore={} # pour restorer la config d'origine en cas d'erreur de génération

    def lockname(self):
        """Nom du lock"""
        return str(self.__class__)

    def lock(self) :
        """ Lock le service courant """
        if not self._locked :
            make_lock(self.lockname(),'')
            self._locked = 1

    def unlock(self) :
        """ Supression du lock """
        if self._locked : remove_lock(self.lockname())

    def __del__(self) :
        # Au cas où...
        self.unlock()

    def _restore(self) :
        """ Affichage d'une erreur et du traceback si debug
        Puis restauration des fichers """
        print ERREUR
        if self.debug :
            import traceback
            traceback.print_exc()
        # Restauration
        for nom, f in self.__restore.items() :
            os.system('cp -f %s %s' % ( f.name, nom ) )

    def _open_conf(self,nom,comment=None) :
        """ Créé un fichier
        si comment est fourni, insère une entète qui utilisera le caractère
        de commentaire fourni

        copie l'ancien fichier dans un fichier temporaire pour permettre
        la restauration en cas d'échec de la configuration

        Retourne le descripteur du fichier """

        f = NamedTemporaryFile()
        os.system('cp %s %s 2> /dev/null' % ( nom, f.name ) )
        self.__restore[nom] = f

        fd = open(nom, 'w')

        if comment :
            e = """***********************************************************
 Ce fichier est genere par les scripts de %s
 Les donnees proviennent de la base LDAP et de la conf
 presente au debut du script.

 Generation : %s
 Fichier : %s

 NE PAS EDITER

***********************************************************""" % \
(__name__, time.strftime('%A %d %B %Y %H:%M'), nom )

            e = comment + e.replace('\n', '\n%s' % comment) + '\n'
            fd.write(e)

        return fd

    def gen_conf(self) :
        """ Génération des fichiers de conf, retourne False si erreur """
        self.lock()
        self.anim = anim('\tgénération fichiers')
        try :
            warn = self._gen()
            if warn :
                self.anim.reinit()
                print WARNING
                if self.debug : sys.stderr.write(warn.encode("UTF-8"))
            else :
                self.anim.reinit()
                print OK
            self.unlock()
            return True
        except :
            self.anim.reinit()
            self._restore()
            self.unlock()
            return False

    def restart(self) :
        """ Redémarrage du service concerné """
        if not self.restart_cmd : return
        self.lock()
        self.anim = anim('\trestart')
        status, output = commands.getstatusoutput(self.restart_cmd)
        if status :
            self.anim.reinit()
            print ERREUR
            if self.debug :
                sys.stderr.write(output+'\n')
            self.unlock()
            return 1
        else :
            print OK
            self.unlock()

    def reconfigure(self) :
        """ Génère les fichiers puis redémarre le service
        si la génération c'est bien passée """
        cprint(u'Reconfiguration %s :' % self.__str__(), 'gras')
        if self.gen_conf() :
            return self.restart()
        else : return 1
