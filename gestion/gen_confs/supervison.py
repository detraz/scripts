#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Licence : GPLv2

import sys, smtplib, commands
sys.path.append('/usr/scripts/gestion')
from ldap_crans import smtpserv, crans_ldap, Machine, Adherent, Club
from whos import machine_details, adher_details, club_details
from gen_confs import gen_config
from affich_tools import cprint, OK, anim
from email.mime.text import MIMEText

class mail:
    """
    Envoie un mail à toutes les personnes de la liste 'To', avec les
    informations détaillées des objets contenus dans 'objets'
    (instances des classes Adherent, Machine ou Club) """
    
    From = 'roots@crans.org'
    To = [ 'roots@crans.org' ]
    Subject = "Surveillance modifications de la base LDAP"

    mail_template =  """From: %(From)s
To: %(To)s
Subject: %(Subject)s
Mime-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Disposition: inline
Content-Transfer-Encoding: 8bit


%(Text)s"""

    # Avec les caractères d'échappement qui vont bien pour la couleur ?
    couleur = False

    def __init__(self,recherches) :
        self.recherches = recherches
        
    def reconfigure(self) :
        """ Envoi le mail """
        cprint(u'Mail de notification de modifications', 'gras')
        a = anim('\tRecherches dans la base',len(self.recherches))
        
        db = crans_ldap()
        details = []
        vus = []
        for rech in self.recherches :
            for results in db.search(rech).values() :
                for res in results :
                    if res.dn in vus : continue
                    vus.append(res.dn)
                    if isinstance(res, Machine):
                        details.append(machine_details(res))
                    elif res.__class__ == Adherent:
                        details.append(adher_details(res))
                    elif res.__class__ == Club:
                        details.append(club_details(res))
            a.cycle()

        texte = '\n\n- - - - = = = = # # # # # # = = = = - - - -\n\n'.join(details)

        a.reinit()
        if not details :
            print "rien"
            return
        
        print OK

        anim('\tEnvoi mail')
        if not self.couleur :
            import re
            texte = re.sub('\x1b\[1;([0-9]|[0-9][0-9])m','',texte)
    
        conn=smtplib.SMTP(smtpserv)
        msg = MIMEText(texte.encode('utf-8'), _charset='utf-8')
        msg['From'] = self.From
        msg['To'] = ','.join(self.To)
        msg['Subject'] = self.Subject
        conn.sendmail(self.From, self.To , msg.as_string())
        conn.quit()
        print OK

class mail_solde:
    """
    Envoie un mail a la ML impression pour les modifications de solde"""
    
    From = 'root@crans.org'
    To = [ 'impression@crans.org' ]
    Subject = "Modification de solde"

    mail_template =  """From: %(From)s
To: %(To)s
Subject: %(Subject)s
X-Mailer: modif_solde (/usr/scripts/gestion/gen_confs/supervision.py)
    
%(Text)s"""

    # Avec les caractères d'échappement qui vont bien pour la couleur ?
    couleur = False

    def __init__(self,modifs) :
        self.modifs = modifs
        
    def reconfigure(self) :
        """ Envoi le mail """
        cprint(u'Mail de notification de modifications du solde', 'gras')
        texte = ''
        for modif in self.modifs:
            texte = texte + modif.decode('utf-8', 'ignore') + u'\n'
        
        anim('\tEnvoi mail')
            
        conn=smtplib.SMTP(smtpserv)
        conn.sendmail(self.From, self.To , \
             self.mail_template % { 'From' : self.From, 
                                    'To' : ','.join(self.To),
                                    'Subject' : self.Subject,
                                    'Text' : texte.encode('utf-8', 'ignore')  } )
        conn.quit()
        print OK
