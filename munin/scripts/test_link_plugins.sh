#!/bin/bash

# Test du liage des plugins par link_plugins.py
# Auteur : Nicolas Dandrimont

TEMPDIR=$(python -c 'import tempfile; print tempfile.mkdtemp()')
python /usr/scripts/munin/scripts/link_plugins.py -d $TEMPDIR

diff -u <(ls /etc/munin/plugins) <(ls $TEMPDIR)

rm -r $TEMPDIR
