#!/usr/bin/env python
# -*- coding: utf-8 -*-
import asterisk
import time
import sys

timeout=10
server="asterisk.adm.crans.org"
port=5038

user="django"
password="HLWkzyITZAmBk"

reload_srv={
  'sip':['chan_sip'],
  'voicemail':['app_voicemail'],
  'dialplan':['pbx_config'],
  'all':['chan_sip','app_voicemail','pbx_config'],
}

manager = asterisk.Manager(user, password, server=server, auto_connect=False, timeout=timeout)
def reload_config(config):
    if not config in reload_srv.keys():
        print >> sys.stderr, "Usage %s [%s]" %(sys.argv[0],'|'.join(reload_srv.keys()))
        return
    manager.connect()
    for module in reload_srv[config]:
        try:manager.reload(module)
        except asterisk.AsteriskError: pass
    manager.logoff()

if __name__ == '__main__' :
    if len(sys.argv)<2:
        print >> sys.stderr, "Usage %s [%s]" %(sys.argv[0],'|'.join(reload_srv.keys()))
        print >> sys.stderr, "Reload config from : \n * %s" % ('\n * '.join(reload_srv.keys()))
        exit(1)
    reload_config(sys.argv[1])

