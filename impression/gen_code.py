#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/impression')
sys.path.append('/usr/scripts/gestion')
from digicode import gen_code
from user_tests import getuser

if __name__ == '__main__':
    # génère un nouveau code et l'affiche
    print gen_code(getuser())
