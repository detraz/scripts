#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

import netsnmp
from utils import sendmail
from time import time
import json
from md5 import md5

STATUS_FILE = "/usr/scripts/var/impression/printer_watch_status.json"
STATUS_MESSAGE_FILE = "/usr/scripts/var/impression/error_message"
PRINTER_HOST = "imprimante.adm.crans.org"

successfuly_imported = False
try:
    with open(STATUS_FILE, "r") as to_load:
        extracted_dict = json.load(to_load)
    try:
        last_status = extracted_dict[u"last_status"]
        message_id = extracted_dict[u"message_id"]
        successfuly_imported = True
    except (KeyError, ValueError):
        pass
except (IOError, ValueError, TypeError):
    pass

# On check le statut de l'imprimante
status_id = netsnmp.Varbind("hrDeviceStatus.1")
status = netsnmp.snmpget(status_id, Version=1, DestHost=PRINTER_HOST, Community="public")

if status == ('5',) and (not successfuly_imported or successfuly_imported and (last_status == '2' or last_status == '3')):
    """
    Cas 1: L'imprimante est down et soit elle était précédemment fonctionnelle ou son statut précédent est inconnu.
    On envoie un mail pour signaler qu'elle est down.
    """

    # On récolte la liste des erreurs dans cette branche
    errors = netsnmp.snmpwalk(netsnmp.Varbind("mib-2.43.18.1.1.8.1"), Version=1, DestHost=PRINTER_HOST, Community="public")
    errors_list = [error + '\n' for error in errors]
    
    #On crée un joli message d'erreur contenant la liste des erreurs
    msg = """ L'imprimante est actuellement hors service. Les erreurs suivantes se sont produites:
    %s""" % ''.join(errors_list)

    #On crée un Message-ID
    message_id = md5(str(time())).hexdigest() + "@imprimante.adm.crans.org"
    
    sendmail.sendmail(u"imprimante.adm@crans.org", u"impression@lists.crans.org", u"Imprimante hors service", msg, more_headers={"X-Mailer": "/usr/scripts/impression/printer_watch.py", "Message-ID": message_id})


elif (status == ('2',) or status == ('3',)) and successfuly_imported and last_status == '5':
    """
    Cas 2: L'imprimante est fonctionnelle après une panne.
    On envoi un mail pour signaler que ce n'est plus la peine de se déplacer.
    """

    msg = " L'imprimante est de nouveau fonctionnelle \o/"
    
    sendmail.sendmail(u"imprimante.adm@crans.org", u"impression@lists.crans.org", u"Imprimante fonctionnelle", msg, more_headers={"X-Mailer": "/usr/scripts/impression/printer_watch.py", "In-Reply-To": message_id})
    message_id = None

elif successfuly_imported:
    pass
else:
    message_id = None

with open(STATUS_FILE, "w") as dump_file:
    json.dump({u"last_status": status[0], u"message_id": message_id}, dump_file)

with open(STATUS_MESSAGE_FILE, 'w') as f:
    if message_id:
        f.write(message_id)
