#!/bin/bash

# Pour dropper les spams en provenance d'un domaine identifié
# sur toutes les MLs

if [ "$(hostname)" != "redisdead" ]
then
   echo "À exécuter sur redisdead."
   exit 1
fi

if [ "$1" = "" ]
then
    echo -e "Usage : discard_from_domain.sh domain.tld\n"
    echo "Droppe les mails en provenance de domain.tld sur toutes les MLs (demande confirmation)"
    exit -1
fi

echo "Recherche…"

targets=$(grep -lRi "$1" /var/lib/mailman/data/heldmsg*)

echo "${targets}"

echo -n "Exterminate ? [y/N] "
read -r ans

case ${ans} in
    y|Y|o|O)
    echo "${targets}" | xargs /var/lib/mailman/bin/discard
;;
    *)
    echo "Aborting"
esac
